package ro.orange;

public class Hotel {
    // variable definition
    public float price;
    public String title;
    public String author;

    public float getPrice() {
        return this.price;
    }

    public String display() {
        return ("The " + this.title + " has the owner " + this.author);
    }
}