package ro.orange;

public class TestHotel {

    public static void main(String[] args) {
        // object instantiation
        Hotel name1 = new Hotel();
        Hotel name2 = new Hotel();
        // instance variables initialization
        name1.title = "Pacific Phhoenix Hotel";
        name2.title = "Ivory Manor Hotel";
        name1.price = 100;
        name2.price = 200;
        name1.author = "Edward Walter";
        name2.author = "Christopher J. Nasetta";

        System.out.println(name1.display());
        System.out.println(name1.getPrice());
        System.out.println(name2.display());
        System.out.println(name2.getPrice());

    }
}